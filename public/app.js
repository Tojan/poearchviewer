
document.addEventListener('DOMContentLoaded', (e) => main())
var canvas
var zoomer
var zoomElement
//id: , childs:[], name:'', desc:'',reward:''
let stack = []
let next = 0
let tree = []
let target
let dropDownData = 0
let dorpDownContainer
let mainContent
let toast
let parts = [
	{ id: 1, childs: [], name: 'Toxic', desc: '', reward: 'Generic - Gem', img: "imgs/toxic.png" },
	{ id: 2, childs: [], name: 'Deadeye', desc: '', reward: 'Generic - Jewellery', img: "imgs/deadeye.png" },
	{ id: 3, childs: [], name: 'Arcane Buffer', desc: '', reward: 'Essence', img: "imgs/arcane-buffer.png" },
	{ id: 4, childs: [], name: 'Echoist', desc: '', reward: 'Generic - Currency', img: "imgs/echoist.png" },
	{ id: 5, childs: [], name: 'Dynamo', desc: '', reward: 'Generic - Jewellery', img: "imgs/dynamo.png" },
	{ id: 6, childs: [], name: 'Bloodletting', desc: 'Items Dropped are corrupted', reward: 'Weapon - Jewellery', img: "imgs/bloodletter.png" },
	{ id: 7, childs: [], name: 'Steel-Infused', desc: '', reward: 'Weapon', img: "imgs/steel-infused.png" },
	{ id: 8, childs: [], name: 'Gargantuan', desc: '', reward: 'Currency', img: "imgs/gargantuan.png" },
	{ id: 9, childs: [], name: 'Berserker', desc: '', reward: 'Unique', img: "imgs/berserker.png" },
	{ id: 10, childs: [], name: 'Sentinel', desc: '', reward: 'Armor - Armor', img: "imgs/sentinel.png" },
	{ id: 11, childs: [], name: 'Vampiric', desc: '', reward: 'Fossil', img: "imgs/vampiric.png" },
	{ id: 12, childs: [], name: 'Soul Conduit', desc: '', reward: 'Map', img: "imgs/soul-conduit.png" },
	{ id: 13, childs: [], name: 'Frenzied', desc: '', reward: 'Generic - Unique', img: "imgs/frenzied.png" },
	{ id: 14, childs: [], name: 'Juggernaut', desc: '', reward: 'Harbinger', img: "imgs/juggernaut.png" },
	{ id: 15, childs: [], name: 'Opulent', desc: '', reward: '?', img: "imgs/opulent.png" },
	{ id: 16, childs: [], name: 'Malediction', desc: '', reward: 'Divination', img: "imgs/malediction.png" },
	{ id: 17, childs: [], name: 'Consecrator', desc: '', reward: 'Fragment', img: "imgs/consecrator.png" },
	{ id: 18, childs: [], name: 'Overcharged', desc: '', reward: 'Jewellery - Jewellery', img: "imgs/overcharged.png" },
	{ id: 19, childs: [], name: 'Chaosweaver', desc: '', reward: 'Gem', img: "imgs/chaosweaver.png" },
	{ id: 20, childs: [], name: 'Frostweaver', desc: '', reward: 'Armor', img: "imgs/frostweaver.png" },
	{ id: 21, childs: [], name: 'Permafrost', desc: '', reward: 'Generic - Armor', img: "imgs/permafrost.png" },
	{ id: 22, childs: [], name: 'Hasted', desc: '', reward: 'Generic', img: "imgs/hasted.png" },
	{ id: 23, childs: [], name: 'Bombardier', desc: '', reward: 'Weapon - Armor', img: "imgs/bombardier.png" },
	{ id: 24, childs: [], name: 'Flameweaver', desc: '', reward: 'Weapon', img: "imgs/flameweaver.png" },
	{ id: 25, childs: [], name: 'Incendiary', desc: '', reward: 'Generic - Weapon', img: "imgs/incendiary.png" },
	{ id: 26, childs: [], name: 'Stormweaver', desc: '', reward: 'Jewellery', img: "imgs/stormweaver.png" },
	{ id: 27, childs: [], name: 'Bonebreaker', desc: '', reward: 'Generic - Weapon', img: "imgs/bonebreaker.png" },
	{ id: 28, childs: [5, 3], name: 'Heralding Minions', desc: '', reward: 'Fragment - Fragment', img: "imgs/heralding-minions.png" },
	{ id: 29, childs: [2, 11], name: 'Assassin', desc: '', reward: 'Currency - Currency', img: "imgs/assassin.png" },
	{ id: 30, childs: [8, 11], name: 'Rejuvenating', desc: 'Rewards are rolled 1 additional time', reward: 'Currency', img: "imgs/rejuvenating.png" },
	{ id: 31, childs: [13, 9], name: 'Executioner', desc: '', reward: 'Legion - Breach', img: "imgs/executioner.png" },
	{ id: 32, childs: [1, 10, 7], name: 'Treant Horde', desc: 'Minions drop random reward', reward: 'Generic', img: "imgs/treant-horde.png" },
	{ id: 33, childs: [4, 12], name: 'Mirror Image', desc: 'Rewards are rolled 2 additional times', reward: 'Scarab', img: "imgs/mirror-image.png" },
	{ id: 34, childs: [1, 6], name: 'Entangler', desc: '', reward: 'Fossil - Fossil', img: "imgs/entangler.png" },
	{ id: 35, childs: [18, 29], name: 'Trickster', desc: '', reward: 'Currency - Unique - Divination', img: "imgs/trickster.png" },
	{ id: 36, childs: [23, 18], name: 'Necromancer', desc: 'Rewards are rolled 2 additional times', reward: 'Generic', img: "imgs/necromancer.png" },
	{ id: 37, childs: [19, 4], name: 'Hexer', desc: '', reward: 'Essence - Essence', img: "imgs/hexer.png" },
	{ id: 38, childs: [16, 2], name: 'Drought Bringer', desc: '', reward: 'Labyrinth - Labyrinth', img: "imgs/drought-bringer.png" },
	{ id: 39, childs: [14, 37, 3], name: 'Temporal Bubble', desc: '', reward: 'Heist - Expedition', img: "imgs/temporal-bubble.png" },
	{ id: 40, childs: [20, 22], name: 'Frost Strider', desc: '', reward: 'Armor - Armor - Armor', img: "imgs/frost-strider.png" },
	{ id: 41, childs: [21, 10], name: 'Ice Prison', desc: '', reward: 'Armor - Armor', img: "imgs/ice-prison.png" },
	{ id: 42, childs: [12, 36, 8], name: 'Soul Eater', desc: '', reward: 'Map - Map', img: "imgs/soul-eater.png" },
	{ id: 43, childs: [24, 22], name: 'Flame Strider', desc: '', reward: 'Weapon - Weapon - Weapon', img: "imgs/flame-strider.png" },
	{ id: 44, childs: [36, 25], name: 'Corpse Detonator', desc: '', reward: 'Divination - Divination', img: "imgs/corpse-detonator.png" },
	{ id: 45, childs: [24, 20, 26], name: 'Evocationist', desc: '', reward: 'Generic - Weapon - Armor - Jewellery', img: "imgs/evocationist.png" },
	{ id: 46, childs: [25, 27], name: 'Magma Barrier', desc: 'Rewards are rolled 2 additional times', reward: 'Weapon - Weapon', img: "imgs/magma-barrier.png" },
	{ id: 47, childs: [26, 22], name: 'Storm Stider', desc: '', reward: 'Jewellery - Jewellery - Jewellery', img: "imgs/storm-strider.png" },
	{ id: 48, childs: [17, 5], name: 'Mana Siphoner', desc: 'Rewards are rolled 1 additional time', reward: 'Jewellery - Jewellery', img: "imgs/mana-siphoner.png" },
	{ id: 49, childs: [6, 19], name: 'Corrupter', desc: 'Items droped are corrupted', reward: 'Abyss - Abyss', img: "imgs/corrupter.png" },
	{ id: 50, childs: [10, 14, 17], name: 'Invulnerable', desc: '', reward: 'Delirium - Metamorph', img: "imgs/invulnerable.png" },
	{ id: 51, childs: [21, 30, 9], name: 'Crystal-Skinned', desc: '', reward: 'Harbinger - Harbinger', img: "imgs/crystal-skinned.png" },
	{ id: 52, childs: [45, 7, 19], name: 'Empowered Elements', desc: 'Rewards are rolled 1 additional time', reward: 'Unique - Unique', img: "imgs/empowered-elements.png" },
	{ id: 53, childs: [37, 16, 49], name: 'Effigy', desc: 'Rewards are rolled 1 additional time', reward: 'Divination - Divination', img: "imgs/effigy.png" },
	{ id: 54, childs: [36, 31, 8], name: 'Empowering Minion', desc: '', reward: 'Blight - Ritual', img: "imgs/empowering-minions.png" },
	{ id: 55, childs: [50, 40, 54], name: 'Lunaris-touched', desc: 'All rewards have an additional reward', reward: 'Unique', img: "imgs/lunaris-touched.png" },
	{ id: 56, childs: [50, 46, 54], name: 'Solaris-touched', desc: 'All rewards have an additional reward', reward: 'Scarab', img: "imgs/solaris-touched.png" },
	{ id: 57, childs: [44, 34, 29], name: 'Arakaali-touched', desc: 'All rewards are divination cards', reward: 'Divination', img: "imgs/arakaali-touched.png" },
	{ id: 58, childs: [41, 47, 28], name: 'Brine King-touched', desc: 'Rewards are rolled 6 additional times', reward: 'Armor - Armor - Armor', img: "imgs/brine-king-touched.png" },
	{ id: 59, childs: [27, 31, 46], name: 'Tukohama-touched', desc: 'Rewards are rolled 4 additional times', reward: 'Weapon - Weapon - Fragment', img: "imgs/tukohama-touched.png" },
	{ id: 60, childs: [43, 13, 30], name: 'Abberath-touched', desc: 'Rewards are rolled 4 additional times', reward: 'Jewellery - Jewellery - Map', img: "imgs/abberath-touched.png" },
	{ id: 61, childs: [34, 42, 38], name: 'Shakari-touched', desc: 'All reward types are unique', reward: 'Unique', img: "imgs/shakari-touched.png" },
	{ id: 62, childs: [55, 56, 33, 48], name: 'Innocence-touched', desc: 'All reward types are currency', reward: 'Currency - Currency - Currency', img: "imgs/innocence-touched.png" },
	{ id: 63, childs: [59, 60, 49, 44], name: 'Kitava-touched', desc: 'Rewards are doubled', reward: 'Generic', img: "imgs/kitava-touched.png" },
	{ id: 64, childs: [33, 32, 29, 30], name: 'Treant-Combo', desc: 'gives nice scarab and currency reward', reward: 'Scarab - Currency - Generic', img:"imgs/brueh.png"},
]
function getPartById(partId) { return (partId >= 1 && partId <= parts.length) ? parts[partId - 1] : null }

function main() {
	mainContent = document.querySelector(".container")
	target = document.querySelector(".tree");
	zoomElement = document.querySelector(".zoom")
	canvas = document.querySelector('.zoomwrapper')
	dorpDownContainer = document.querySelector(".dropdownchart")
	toast = document.querySelector(".toast")

	canvas.addEventListener('mousedown', (e) => {

		dorpDownContainer.style.display = 'none'
	})

	zoomer = panzoom(zoomElement, {
		beforeWheel: (e) => { return e.altKey || e.shiftKey || e.ctrlKey },
		maxZoom: 2,
		minZoom: 0.2
	})

	var list = document.querySelector(".itemlist")
	parts.forEach((e) => {
		if (e.childs.length != 0) {

			let wrapper = document.createElement("div")

			let button = document.createElement("button")
			button.innerText = e.name;
			button.addEventListener('click', (e) => loadTree(e.target.id))
			button.id = e.id
			wrapper.appendChild(button)

			let text1 = document.createElement("span")
			text1.innerText = e.reward
			wrapper.appendChild(text1)

			let text2 = document.createElement("span")
			text2.classList.add('desc')
			text2.innerText = e.desc
			wrapper.appendChild(text2)

			list.prepend(wrapper);
		}

	})

	if (!!(document.location.search)) {
		document.location.search.substring(1).split('&').forEach(kv => {
			let parts = kv.split('=');
			if (parts[0] === 'partid') {
				loadTree((parts[1] | 0)); //force id to be numeric
			}
		})
	}

	document.querySelector('#e1').addEventListener('click', getSearchString)
	document.querySelector('#e2').addEventListener('click', getChildsSearchString)
	document.querySelector('#e3').addEventListener('click', drawSubTree)

	fillSearchSuggestions()
	document.querySelector('#searchQuery').addEventListener('focus', showSearchSuggestion)
	document.querySelector('#searchQuery').addEventListener('blur', hideSearchSuggestion)
	document.querySelector('#searchQuery').addEventListener('keyup', filterFunction)

}

function loadTree(partId) {
	//clear / reset previous stuff
	stack = []
	next = 0
	tree = []
	target.innerHTML = "";
	zoomer.dispose()

	//build tree
	history.replaceState({}, '', '?partid=' + partId)
	stack.push(target)
	appendTree(partId)
	stack.pop()

	//zoom and fit into screen?
	zoomer = panzoom(zoomElement, {
		beforeWheel: (e) => { return e.altKey || e.shiftKey || e.ctrlKey },
		maxZoom: 2,
		minZoom: 0.2
	})
	zoomer.on('transform', (e) => {
		dorpDownContainer.style.display = 'none'
	});
	zoomer.on('panstart', function (e) {
		dropDownData = -99
	});
	zoomer.on('panend', (e) => {
		window.setTimeout(() => { dropDownData = 0 }, 50)
	})

}
function appendTree(partId) {
	next += 1
	let container = stack.slice(-1)[0];

	//build metadata pack
	let entry = { id: next, partId: partId }
	//try to get parent node
	if (container.parentElement != null) {
		entry.pid = container.parentElement.dataset.id
	}

	//generate dom
	entry.node = document.createElement('li')

	//insert into dom tree
	container.appendChild(entry.node)

	entry.node.dataset.id = next
	let contentWrapper = document.createElement('span')
	contentWrapper.classList.add('node')
	entry.node.appendChild(contentWrapper)

	let contentCard = document.createElement('div')
	contentCard.classList.add('partCard')
	contentCard.dataset.id = partId
	contentCard.addEventListener('click', showDropDown)
	contentCard.appendChild

	contentWrapper.appendChild(contentCard)

	renderNode(entry)

	//process children
	stack.push(document.createElement('ul'))
	getPartById(partId).childs.forEach(child => appendTree(child))
	let childsContainer = stack.pop()
	if (childsContainer.childElementCount > 0) {
		entry.node.appendChild(childsContainer);
	}

}

function getSearchString(e) {
	var name = getPartById(dropDownData).name
	name = name.substring(0, 3)
	navigator.clipboard.writeText("^(" + name + ")")
	dorpDownContainer.style.display = 'none'
	toasten("copied to clipboard")
}
function getChildsSearchString(e) {
	var childs = getPartById(dropDownData).childs
	let text = '^(' + childs.map(id => getPartById(id).name.substring(0, 3)).join('|') + ')'
	navigator.clipboard.writeText(text)
	dorpDownContainer.style.display = 'none'
	toasten("copied to clipboard")
}
function drawSubTree(e) {
	loadTree(dropDownData)
}
function renderNode(entry) {
	let body = entry.node.firstChild.firstChild;
	let data = getPartById(entry.partId);

	let template = document.querySelector('#fancyCheckbox')
	let clone = document.importNode(template.content, true)
	body.appendChild(clone)

	body.querySelector('input').addEventListener('click', toggleChilds)

	let bigimg = document.createElement('img')
	bigimg.classList.add('big')
	bigimg.src = data.img;
	body.appendChild(bigimg)

	let header = document.createElement('span')
	header.classList.add('name')
	header.innerText = data.name;
	body.appendChild(header);

	let desc = document.createElement('span')
	desc.classList.add('desc')
	data.reward.split(' - ').forEach(r => {
		let img = document.createElement('img')
		img.src = 'imgs/' + r + '.png'
		desc.appendChild(img)
	})
	desc.appendChild(document.createElement('br'))
	if (data.desc) {
		let text = document.createElement('span')
		text.innerText = data.desc
		desc.appendChild(text)
	}
	body.appendChild(desc)
}
function showDropDown(e) {
	if (dropDownData < 0) {
		return
	}
	let card = e.target
	while (!card.classList.contains('partCard')) {
		card = card.parentElement
	}
	dropDownData = card.dataset.id

	dorpDownContainer.style.left = e.pageX + 'px'
	dorpDownContainer.style.top = e.pageY + 'px'
	dorpDownContainer.style.display = 'block'
}
function playanim(elem, anim, post, displayOverride) {
	elem.style.animation = '';
	window.setTimeout(() => {
		elem.style.display = (typeof (displayOverride) !== 'undefined' ? displayOverride : '');
		elem.style.animation = anim;
		if (typeof (post) !== 'undefined') elem.addEventListener('animationend', post, { once: true });
	}, 1);
}
function toasten(msg) {
	toast.innerText = msg;
	playanim(toast, '1s linear 0s anim_popfade', (e) => toast.style.display = 'none', 'block');
}
function toggleChilds(e) {
	e.stopImmediatePropagation()
	let node = e.target
	while (node.tagName != 'LI') {
		node = node.parentElement
	}
	let subTree = node.querySelector('ul')
	if (e.target.checked) {
		subTree.style.display = 'none'
	} else {
		subTree.style.display = ''
	}
}
function filterFunction() {
	var input, filter, ul, li, a, i;
	input = document.getElementById("searchQuery");
	filter = input.value.toUpperCase();
	div = document.getElementById("myDropdown");
	a = div.getElementsByTagName("a");
	for (i = 0; i < a.length; i++) {
		txtValue = a[i].textContent || a[i].innerText;
		if (txtValue.toUpperCase().indexOf(filter) > -1) {
			a[i].style.display = "";
		} else {
			a[i].style.display = "none";
		}
	}
}
function showSearchSuggestion(e){
	document.getElementById("myDropdown").style.display = 'block'
}
function hideSearchSuggestion(e){
	document.getElementById("myDropdown").style.display = 'none'
}
function fillSearchSuggestions(){
	let dropDown = document.querySelector('#myDropdown')
	parts.forEach(element => {
		let a = document.createElement('a')
		a.href = 'javascript:void(0)'
		a.innerText = element.name
		dropDown.appendChild(a)
		a.addEventListener('mousedown', () => loadTree(element.id))
	});
	
}
function test(e){
	console.log('test')
}